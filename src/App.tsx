import { Container } from '@material-ui/core'

import { Counter } from 'components'
import { StoreProvider } from 'store'
import { StyleProvider } from 'styles'

const App = () => (
  <StoreProvider>
    <StyleProvider>
      <Container component="main">
        <Counter/>
      </Container>
    </StyleProvider>
  </StoreProvider>
)

export default App