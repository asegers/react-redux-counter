import React from 'react'
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

const THEME = createMuiTheme({
    palette: {
        type: "dark",
    }
})

const StyleProvider: React.FC = ({children}) => {
    return (
        <ThemeProvider theme={THEME}>
            <CssBaseline/>
            {children}
        </ThemeProvider>
    )
}

export default StyleProvider
