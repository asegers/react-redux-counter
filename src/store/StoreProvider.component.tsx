import { configureStore } from '@reduxjs/toolkit'
import { Provider } from 'react-redux'
import { combineReducers } from 'redux'
import logger from 'redux-logger'

import { reducer as counterReducer } from './counter'

export const rootReducer = combineReducers({
    counter: counterReducer,
});

export type RootState = ReturnType<typeof rootReducer>

const STORE = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
  devTools: process.env.NODE_ENV !== 'production',
})

const StoreProvider: React.FC = ({children}) => (
    <>
        <Provider store={STORE}>
            {children}
        </Provider>
    </>
)

export default StoreProvider;