import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface CounterState { count: number }

const initialState = { count: 0 } as CounterState

const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment(state) {
      state.count++
    },
    decrement(state) {
      state.count--
    },
    set(state, action: PayloadAction<number>) {
      state.count = action.payload
    },
  },
})

export const { actions, reducer } = counterSlice
export default counterSlice