import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
// import { createSelector } from 'reselect'
import {
    Card, CardActions, CardContent,
    Box,
    Button, ButtonGroup,
    TextField,
} from '@material-ui/core'
import {
    KeyboardArrowUp as KeyboardArrowUpIcon,
    KeyboardArrowDown as KeyboardArrowDownIcon,
} from '@material-ui/icons'

import { actions as A } from 'store/counter'

const Counter = () => {
    const { count } = useSelector(state => state.counter)
    const dispatch = useDispatch()

    const handleSet = ({ currentTarget: { value } }: React.ChangeEvent<HTMLInputElement>) => {
        const newCount = parseInt(value);
        
        if (newCount === NaN) {
            dispatch(A.set(0));
        } else {
            dispatch(A.set(parseInt(value)));
        }
    }
    const handleIncr = () => {
        dispatch(A.increment())
    }
    const handleDecr= () => {
        dispatch(A.decrement())
    }

    return (
        <Card style={{width: "350px", margin: "50px auto 0"}}>
            <Box display="flex" justifyContent="center">
                <CardContent>
                    <form>
                        <TextField fullWidth onChange={handleSet} value={count} variant="outlined"/>
                    </form>
                </CardContent>
                <CardActions>
                    <ButtonGroup orientation="vertical" color="primary">
                        <Button onClick={handleIncr}><KeyboardArrowUpIcon/></Button>
                        <Button onClick={handleDecr}><KeyboardArrowDownIcon/></Button>
                    </ButtonGroup>
                </CardActions>
            </Box>
        </Card>
    )
}

export default Counter
